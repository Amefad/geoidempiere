/*
	L.panzoom adds control for pan e zoombox
*/




/*
 * L.Handler.ShiftDragZoom is used to add shift-drag zoom interaction to the map
  * (zoom to a selected bounding box), enabled by default.
 */
//modifica ame inizio
var messaggio;

//modifica ame fine
 
 
L.Map.mergeOptions({
	zoomBox: false,
	tZoomBox: true
});


L.Map.TouchZoomBox = L.Map.TouchZoom.extend ({
	_onTouchStart: function(e){
		L.Map.ZoomBox._onMouseDown;
	},
	_onTouchMove: function(e){
		L.Map.ZoomBox._onMouseMove;
	},
	_onTouchEnd: function(e){
		L.Map.ZoomBox._onMouseUp;
	}
});



L.Map.addInitHook('addHandler', 'tZoomBox', L.Map.TouchZoomBox);


L.Map.ZoomBox = L.Map.BoxZoom.extend({

	_onMouseDown: function (e) {
		
		//alert('mousedown');

		L.DomUtil.disableTextSelection();

		this._startLayerPoint = this._map.mouseEventToLayerPoint(e);

		this._box = L.DomUtil.create('div', 'leaflet-zoom-box', this._pane);
		L.DomUtil.setPosition(this._box, this._startLayerPoint);

		//TODO refactor: move cursor to styles
		this._container.style.cursor = 'crosshair';

		L.DomEvent
		    .on(document, 'mousemove', this._onMouseMove, this)
		    .on(document, 'mouseup', this._onMouseUp, this)
		    .preventDefault(e);

		this._map.fire("boxzoomstart");
	},
		
		_onMouseUp: function (e) {
		//alert('mouseup');
		this._pane.removeChild(this._box);
		
		L.DomUtil.enableTextSelection();

		L.DomEvent
		    .off(document, 'mousemove', this._onMouseMove)
		    .off(document, 'mouseup', this._onMouseUp);

		var map = this._map,
		    layerPoint = map.mouseEventToLayerPoint(e);

		if (this._startLayerPoint.equals(layerPoint)) { return; }

		var bounds = new L.LatLngBounds(
		        map.layerPointToLatLng(this._startLayerPoint),
		        map.layerPointToLatLng(layerPoint));

		map.fitBounds(bounds);

		map.fire("boxzoomend", {
			boxZoomBounds: bounds
			
		});
	}


});
L.Map.addInitHook('addHandler', 'zoomBox', L.Map.ZoomBox);
//per attivare lo zoom
//map.dragging.disable()
//map.zoomBox.enable()

/*code from christina Herzog
leaflet group
*/


 
