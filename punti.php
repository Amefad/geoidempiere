<?php
/**
 * Title:   PostGIS to GeoJSON
 * Notes:   Query a PostGIS table or view and return the results in GeoJSON format, suitable for use in OpenLayers, Leaflet, etc.
 * Author:  Bryan R. McBride, GISP
 * Contact: bryanmcbride.com
 * GitHub:  https://github.com/bmcbride/PHP-Database-GeoJSON
 */
 
 // Amedeo Fadini fame@libero.it  per ConsulNet 2015


 //other parameters
 $table = 'adempiere.lit_geo';
 $geomfield = 'point_geom';
require_once 'connect_db.php';
$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
$drivername = $conn->getAttribute(PDO::ATTR_DRIVER_NAME);
$user = $_REQUEST['user'];
$layer = $_REQUEST['layer'];
$where = "WHERE ad_user_id =".$user;
switch ($drivername){
	case 'sqlite' :
	$geomfield = 'GEOMETRY';
	$sql = "SELECT *, '{\"type\":\"Point\",\"coordinates\":['||lon||','||lat||']}' as geojson FROM  $table $where";
	break;
	
	default:
	$sql = "SELECT *, ST_AsGeoJson($geomfield) as geojson  FROM  $table $where";

}

#prepare statement and bind parameter
#echo $sql;
$stmt = $conn->prepare($sql);

#todo fix prepared statement to include bind params

# Try query or error
$rs = $stmt->execute();
if (!$rs) {
    echo 'An SQL error occured.\n';
    exit;
}

# Build GeoJSON feature collection array
$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);
# Loop through rows to build feature arrays
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $properties = $row;
    # Remove geojson and geometry fields from properties
    unset($properties['geojson']);
    unset($properties[$geomfield]);
    $feature = array(
         'type' => 'Feature',
         'geometry' => json_decode($row['geojson'], true),
         'properties' => $properties
    );
    # Add feature arrays to feature collection array
    array_push($geojson['features'], $feature);
}
#header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
$conn = NULL;
?>
