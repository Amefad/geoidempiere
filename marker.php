<?php
/**
 * Title:   PostGIS to Marker
 * Notes:   Query a PostGIS table or view and return a leaflet.js marker based on lat lon value, suitable for use in OpenLayers, Leaflet, etc.
 * Author:  Amedeo Fadini fame@libero.it  per ConsulNet 2016
 * Credit: bryanmcbride.com GitHub:  https://github.com/bmcbride/PHP-Database-GeoJSON
 */
 
 // Amedeo Fadini fame@libero.it  per ConsulNet 2016


 //connection parameters
 $host = 'localhost';
 $user = 'idempiere';
 $pwd = 'idempiere';
 $db = 'geoidempiere';
 $table = 'clienti_geom';
 $geomfield = 'point_geom';
 $id = 1;

 
 //Parameters from get string or post data
@$id = $_REQUEST['id'];
# @$filter = split('+', $_REQUEST['where']);
#  echo $filter[1];

require_once 'connect_db.php';

# Build SQL SELECT statement and return the geometry as a GeoJSON element
$where = "WHERE table_name='clienti' AND pk_value= :id";
$sql = "SELECT lat, lon, pk_value FROM  $table $where LIMIT 1";
#print $sql;
$stmt = $conn->prepare($sql);
$stmt->bindParam(':id', $id);
# Try query or error
$rs = $stmt->execute();

if (!$rs) {
    echo 'An SQL error occured.\n';
    exit;
}

# Get row array
$row = $stmt->fetch(PDO::FETCH_ASSOC);
header('Content-type: text/javascript');
echo 'markid = new L.marker(['.$row['lat'].','.$row['lon'].']); ';
  
$conn = NULL;
?>
