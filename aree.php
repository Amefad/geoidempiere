<?php
/**
 * Title:   PostGIS to GeoJSON
 * Notes:   Query a PostGIS table or view and return the results in GeoJSON format, suitable for use in OpenLayers, Leaflet, etc.
 * Author:  Bryan R. McBride, GISP
 * Contact: bryanmcbride.com
 * GitHub:  https://github.com/bmcbride/PHP-Database-GeoJSON
 */
 
 // Amedeo Fadini fame@libero.it
 //only for debug
 $db = 'idetestdb';

 
//Parameters from get string or post data
 @$db = $_REQUEST['db'];
 
 //other parameters
 $table = 'aree';
 $geomfield = 'the_geom4326';
 $host = 'localhost';
 $user = '';
# Connect to PostgreSQL database
$conn = new PDO('pgsql:host=178.32.140.162;dbname='.$db,'wms','PPccte22e');

# Build SQL SELECT statement and return the geometry as a GeoJSON element
$sql = "SELECT *, public.ST_AsGeoJSON($geomfield) as geojson FROM  $table";

# Try query or error
$rs = $conn->query($sql);
if (!$rs) {
    echo 'An SQL error occured.\n';
    exit;
}

# Build GeoJSON feature collection array
$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);

# Loop through rows to build feature arrays
while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
    $properties = $row;
    # Remove geojson and geometry fields from properties
    unset($properties['geojson']);
    unset($properties[$geomfield]);
    $feature = array(
         'type' => 'Feature',
         'geometry' => json_decode($row['geojson'], true),
         'properties' => $properties
    );
    # Add feature arrays to feature collection array
    array_push($geojson['features'], $feature);
}

header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
$conn = NULL;
?>
