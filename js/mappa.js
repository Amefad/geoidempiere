/***********************************
Author: Amedeo Fadini per Consulnet 
E-mail: fame@libero.it
Date start: ottobre 2015
License: Free BSD http://opensource.org/licenses/BSD-2-Clause

Copyright (c) 2016, Amedeo Fadini Consulnet  
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/
var map;


/*******************************
BASE LAYERS
*****************************/

var mapquestOSM = new L.TileLayer("http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png", {
	maxZoom: 17,
	subdomains: ["otile1", "otile2", "otile3", "otile4"],
	attribution: 'Tiles courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a>. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});
var googleLayer = new L.Google(); 
var bingLayer = new L.BingLayer("AndoO6AeN_kKQeM6pHMG-r-9ZhlpYJ5ug5ObUQELdZrIksuKMEF8r2K7DAe6ZQFA");


/*******************************
STYLES AND ICON
*****************************/
//styles and Icons
		var confini_vuoto = {
			"color": "red",
			"weight": 3,
			"fillOpacity": 0.0
		};
		var linee_style = {
			"color": "green",
			"weight": 3
		};	
		var aree_style = {
			"color": "blue",
			"weight": 3,
			"fillOpacity": 0.8
		};				
		
		var segnaIcon = L.icon({
			iconUrl: './icone/segnalazione.png',
			iconSize: [23, 26],
			iconAnchor: [0, 26],
			popupAnchor: [11, -20]
		});
		var markIcon = L.icon({
			iconUrl: './icone/marker.png',
			iconSize: [18, 26],
			iconAnchor: [9, 26],
			popupAnchor: [9, 26]
		});
		

		
/*******************************
OVERLAYS
*****************************/
//overlays
//comuni geoJson
var comuni = new L.geoJson(null,{ 
	style: confini_vuoto
	
	});
  $.getJSON("layers/confini4326.geojson", function(data){
        comuni.addData(data);
		comuni.eachLayer(function(layer){
			//alert(layer.getBounds().getCenter().toString());
			//layer._container.style.pointerEvents='none'	
		});
    });
    
//markercluster

var markers = L.markerClusterGroup();




//layer contatti
var layer = ['contatti'];
var contatti  = new L.geoJson(null, {
			pointToLayer: function(feature, latlon) {
				nuovaIcona = new L.Icon({
					iconUrl: 'icone/marker-icon-2x-'+feature.properties.color+'.png',
					shadowUrl: 'icone/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				});
				return L.marker(latlon, {icon: nuovaIcona});
				//return L.circleMarker(latlon, {radius: 10});
			},
				onEachFeature: popupFeature
		});

//l'indirizzo andr� sostituito da una chiamata php
var jsondata;
var layerfile = 'punti.php?user=' + user + '&layers=' + layer[0];
//decommentare in locale
//layerfile = 'punti.php.json';
$.getJSON(layerfile, function(data) {
	jsondata = data;	//New GeoJSON layer
	contatti.addData(jsondata);
	map.addLayer(contatti);
	map.fitBounds(contatti.getBounds());
});
//var layerfile = 'punti.php?user=' + user + '&layers=' + layer[0];


  

//funzione creazione popup per tutte le feature di layer geojson
function popupFeature(feature, layer) {

var popupContent = "<h3>" +	feature.properties.name + "</h3>";
			
var p = feature.properties;			
for (var key in p) {
	var printable = $.inArray(key, ["name", "ad_client_id", "lat", "lon", "ad_org_id", "color", "icon", "ad_user_id", "layer", "dimension", "lit_geographicobject_id"]) < 0;
  if (p.hasOwnProperty(key) && printable==true) {
	  
    popupContent = popupContent + key + ": " + p[key] + "<br>";
  }
}
			
	
		layer.bindPopup(popupContent);
};





/*******************************
CREATE MAP
*****************************/
//markid � un oggetto di tipo marker che viene passato dalla pagina marker.php
//todo: la mappa non viene centrata automaticamente
		map = new L.Map('map', {
			
			zoom: 10,
			layers: [mapquestOSM],
			zoomControl: true,
			inertia: false
		});



		

		//map.fitBounds(markers.getBounds());

//layergroup e aggiunta layers
		var baseLayers = {
			"Bing" : bingLayer
			,"MapQuest Streets": mapquestOSM
			,"Google Satellite": googleLayer
			
		};
		var overlays = {
			"Comuni": comuni
			,"Contatti" : contatti
		};
//layercontrol
		layersControl = new L.Control.Layers(baseLayers,  overlays, {
		collapsed: true
		});
map.addControl(layersControl); 
		//crea un layergroup dove aggiungere i marker di segnalazione per l'inserimento di nuovi punti
		var segnalazione = new L.LayerGroup()
			.addTo(map);



		
/*******************************
MARKERS
*****************************/
		//crea un marker di segnalazione sulla mappa
		var segnala = function(e) { 
			//rimuove eventuali markers presenti dal layergroup
			segnalazione.clearLayers();
			//alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);
			var latitude = e.latlng.lat.toFixed(5);
			var longitude = e.latlng.lng.toFixed(5);
			//crea un marker e lo rende trascinabile
			segnalino = L.marker([latitude, longitude], {icon: markIcon});
			//aggiunge un layer al layergroup
			segnalazione.addLayer(segnalino);
			segnalino.dragging.enable();
			//le righe seguenti servono per compilare i campi di un form con latitudine e longitudine del segnalino
			//document.getElementById("lat").value = latitude;
			//document.getElementById("lon").value = longitude;
			//alert("lat" + latitude + "; long" + longitude);
		};
		
/*******************************
FUNCTION
*****************************/
//due funzioni per attivare e disattivare il controllo personalizzato e l'inserimento di coordinate
				function attiva_coordinate(){
				map.on('click', segnala);
				//confini.on('click', segnala);
			   };
			   	function disattiva_coordinate(){
				map.off('click', segnala);
				//confini.off('click', segnala);
			   };
			   function hidepoint() {
			   segnalazione.clearLayers();
			   disattiva_coordinate();
			   };
			

	
//locate control controllo di geolocalizzazione dal browser
locateControl = L.control.locate({
    position: 'bottomleft',  // set the location of the control
    drawCircle: true,  // controls whether a circle is drawn that shows the uncertainty about the location
    follow: true,  // follow the location if `watch` and `setView` are set to true in locateOptions
    circleStyle: {},  // change the style of the circle around the user's location
    markerStyle: {},
    metric: true,  // use metric or imperial units
    onLocationError: function(err) {alert(err.message)}  // define an error callback function
});
locateControl.addTo(map);

//map.fitBounds(confini.getBounds());
//controllo legenda
LegendaControl = function(livelliFunction) {
    var control = new (L.Control.extend({
    options: { position: 'topright' },
    onAdd: function (map) {
        controlDiv = L.DomUtil.create('div', 'legenda-button');
        L.DomEvent
            .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
            .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
            .addListener(controlDiv, 'click', this.LegendaFunction);
		
		legendadiv = controlDiv;
        return controlDiv;
    }
    }));
    control.LegendaFunction = livelliFunction;
    return control;
};
LegendaFunction = function () {
	layersControl._expand();
};
map.addControl(LegendaControl(LegendaFunction));

//controllo per attivare o disattivare lo zoom su riquadro
PanZoomBoxControl = function(theZoomBoxFunction) {
    var control = new (L.Control.extend({
    options: { position: 'topright' },
    onAdd: function (map) {
        controlDiv = L.DomUtil.create('div', 'zoom-button');
        L.DomEvent
            .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
            .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
            .addListener(controlDiv, 'click', this.ZoomBoxFunction);
		panzoomdiv = controlDiv;
        return controlDiv;
    }
    }));
    control.ZoomBoxFunction = theZoomBoxFunction;
    return control;
};
ZoomBoxFunction = function () {
	//alert(map.dragging._enabled);
			if(map.dragging._enabled == true){
				map.dragging.disable(),
				map.zoomBox.enable()
				panzoomdiv.className= "zoom-button-active leaflet-control"
				map._container.style.cursor = 'crosshair'
				}else{
				map.dragging.enable(),
				map.zoomBox.disable(),
				panzoomdiv.className= "zoom-button leaflet-control"
				map._container.style.cursor = ''
				};
};
map.addControl(PanZoomBoxControl(ZoomBoxFunction));
map.on('boxzoomend', function(e) {
    ZoomBoxFunction();
});







